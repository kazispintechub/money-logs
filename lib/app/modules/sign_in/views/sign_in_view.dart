import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/theme/button_theme.dart';
import '../../../core/theme/input_theme.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../controllers/sign_in_controller.dart';

class SignInView extends GetView<SignInController> {
  final SignInController _signInController = Get.find<SignInController>();

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appbar: AppBar(
        toolbarHeight: 0,
        shadowColor: transparent,
        backgroundColor: transparent,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              SizedBox(height: 100),
              Image.asset("assets/images/money_logs_logo.png", height: 100),
              SizedBox(height: 20),
              Text("Welcome Back", style: TextStyles.medium25Black),
              SizedBox(height: 40),
              Form(
                key: _signInController.signInFormKey,
                child: Column(
                  children: [
                    usernameForm(),
                    passwordForm(),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Obx(
                () => _signInController.isLoading.value
                    ? Center(child: CircularProgressIndicator())
                    : SubmitButton(
                        onTap: () {
                          _signInController.signIn();
                        },
                        buttonColor: buttonGreen,
                        title: "Sign In",
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  CustomInputField usernameForm() {
    return CustomInputField(
      hint: 'Your User Name',
      controller: _signInController.usernameCtl,
      validator: (username) {
        return _signInController.usernameValidation(username);
      },
      onSaved: (username) {
        _signInController.usernameCtl.text = username;
      },
    );
  }

  CustomInputField passwordForm() {
    return CustomInputField(
      hint: 'Your Secret',
      controller: _signInController.passwordCtl,
      validator: (password) {
        return _signInController.passwordValidation(password);
      },
      onSaved: (password) {
        _signInController.passwordCtl.text = password;
      },
      isHidden: true,
    );
  }
}
