import 'dart:developer';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../data/providers/db_provider.dart';

class ChartController extends GetxController {
  final RxDouble incomeValue = 0.0.obs;
  final RxDouble expenseValue = 0.0.obs;
  final RxString selectedMonth = "Life Time".obs;

  @override
  onInit() {
    getIncomeValueByFilter(selectedMonth.value);
    super.onInit();
  }

  // MyBarChartData(id: 0, headName: "Salary (Income)", y: 10, type: 1),
  // MyBarChartData(id: 1, headName: "Transport (Expense)", y: 55, type: 2),
  // MyBarChartData(
  // id: 2, headName: "Medical Allowance (Income)", y: 20, type: 1),
  getIncomeValueByFilter(String filterValue) async {
    var year = DateTime.now().year;
    switch (filterValue) {
      case "January":
        // log(DateTime(year, 2, 0).toString());
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-01-01", "$year-01-31");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-01-01", "$year-01-31");
        update();
        break;
      case "February":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-02-01",
                DateFormat('yyyy-MM-dd').format(DateTime(year, 3, 0)));
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-02-01",
                DateFormat('yyyy-MM-dd').format(DateTime(year, 3, 0)));
        break;
      case "March":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-03-01", "$year-03-31");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-03-01", "$year-03-31");
        break;
      case "April":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-04-01", "$year-04-30");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-04-01", "$year-04-30");
        break;
      case "May":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-05-01", "$year-05-31");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-05-01", "$year-05-31");
        break;
      case "June":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-06-01", "$year-06-30");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-06-01", "$year-06-30");
        break;
      case "July":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-07-01", "$year-07-31");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-07-01", "$year-07-31");
        break;
      case "August":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-08-01", "$year-08-31");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-08-01", "$year-08-31");
        break;
      case "September":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-09-01", "$year-09-30");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-09-01", "$year-09-30");
        break;
      case "October":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-10-01", "$year-10-31");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-10-01", "$year-10-31");
        break;
      case "November":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-11-01", "$year-11-30");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-11-01", "$year-11-30");
        break;
      case "December":
        incomeValue.value = await MoneyLogsDatabase.instance
            .filteredTotalIncome("$year-12-01", "$year-12-31");
        expenseValue.value = await MoneyLogsDatabase.instance
            .filteredTotalExpense("$year-12-01", "$year-12-31");
        break;
      default:
        incomeValue.value = await MoneyLogsDatabase.instance.totalIncome();
        expenseValue.value = await MoneyLogsDatabase.instance.totalExpense();
        log(incomeValue.value.toString());
        log(expenseValue.value.toString());
        update();
        break;
    }
  }
}
