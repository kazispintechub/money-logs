import 'package:get/get.dart';

import '../../../data/services/settings_service.dart';

class OnBoardingController extends GetxController {
  final SettingsService settingsService;

  OnBoardingController({required this.settingsService});
}
