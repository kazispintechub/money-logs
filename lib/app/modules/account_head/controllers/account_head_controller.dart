import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/core/models/account_sub_head_model.dart';

import '../../../data/providers/db_provider.dart';

class AccountHeadController extends GetxController {
  final TextEditingController accountHeadCtl = TextEditingController();
  final TextEditingController accountHeadAmountCtl = TextEditingController();
  final TextEditingController accountSubHeadCtl = TextEditingController();

  final RxInt selectedACHeadType = 0.obs;

  // final RxList<AccountHeadFirstList> accountHeadList = [
  //   AccountHeadFirstList(
  //     name: "Salary (Income)",
  //     add: true.obs,
  //     type: AccountHeadType.income.index,
  //   ),
  //   AccountHeadFirstList(
  //     name: "Transport Allowance (Income)",
  //     add: true.obs,
  //     type: AccountHeadType.income.index,
  //   ),
  //   AccountHeadFirstList(
  //     name: "Medical Allowance (Income)",
  //     add: true.obs,
  //     type: AccountHeadType.income.index,
  //   ),
  //   AccountHeadFirstList(
  //     name: "Pocket Money (Income)",
  //     add: true.obs,
  //     type: AccountHeadType.income.index,
  //   ),
  //   AccountHeadFirstList(
  //     name: "Food (Expense)",
  //     add: true.obs,
  //     type: AccountHeadType.expense.index,
  //   ),
  //   AccountHeadFirstList(
  //     name: "Transport (Expense)",
  //     add: true.obs,
  //     type: AccountHeadType.expense.index,
  //   ),
  //   AccountHeadFirstList(
  //     name: "Medical (Expense)",
  //     add: true.obs,
  //     type: AccountHeadType.expense.index,
  //   ),
  // ].obs;

  // addNow(head, type, amount) async {
  //   await MoneyLogsDatabase.instance.addAccountHead(
  //     AccountHead(
  //       title: head.name!,
  //       headAmount: amount,
  //       typeId: type,
  //       createdBy: 1,
  //       createAt: DateFormat('dd-MM-yyyy').format(DateTime.now()),
  //     ),
  //   );
  // }

  // addAcHeads() {
  //   for (var head in accountHeadList) {
  //     log(head.name!);
  //     if (head.add.isTrue) {
  //       addNow(head, head.type);
  //     }
  //   }
  //   Get.offNamedUntil("/dashboard", (route) => false);
  //   Get.snack("New Heads", "New Head Added");
  // }

  // Future<bool> checkCanDelete(AccountHead data) async {
  //   var result = await MoneyLogsDatabase.instance.showDeleteButton(data);
  //   log(result.toString());
  //   return result;
  // }

  deleteHead(head) async {
    await MoneyLogsDatabase.instance.deleteAccountHead(head);
    update();
  }
}
