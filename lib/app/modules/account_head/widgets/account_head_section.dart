import 'dart:developer';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/models/account_sub_head_model.dart';
import '../../../core/theme/button_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../../../data/providers/db_provider.dart';
import '../controllers/account_head_controller.dart';
import 'account_sub_head_section.dart';

class AccountHeadSection extends GetView<AccountHeadController> {
  final AsyncSnapshot<List<AccountHead>> accHead;
  final AccountHeadController _accountHeadController =
      Get.find<AccountHeadController>();

  AccountHeadSection({Key? key, required this.accHead}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: accHead.data!
            .map(
              (e) => ExpansionTile(
                leading: Icon(Icons.account_balance_wallet_outlined),
                title: Text(
                  e.title,
                  style: e.typeId == 1
                      ? TextStyles.normal16Green
                      : TextStyles.normal16Red,
                ),
                subtitle: Text(
                  "${e.headAmount} TK",
                  style: e.typeId == 1
                      ? TextStyles.normal12Green
                      : TextStyles.normal12Red,
                ),
                children: [
                  FutureBuilder(
                    future:
                        MoneyLogsDatabase.instance.getSubHeadsByHeadId(e.id!),
                    builder: (
                      BuildContext context,
                      AsyncSnapshot<List<AccountSubHead>> snapshot,
                    ) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                          return errorSection();
                        } else if (snapshot.data!.isEmpty) {
                          return noDataSection();
                        } else if (snapshot.hasData) {
                          log(snapshot.data.toString());
                          return AccountSubHeadSection(
                            accountSubHead: snapshot.data!,
                          );
                        }
                      }
                      return const Center(child: CircularProgressIndicator());
                    },
                  ),
                  SubmitButton(
                    title: "Add Sub Head",
                    onTap: () {
                      addCategoryDialog(e);
                    },
                    buttonColor: e.typeId == 1 ? buttonGreen : buttonRed,
                  )
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  addCategoryDialog(AccountHead accountHead) {
    String myTitle = "";
    if (accountHead.typeId == 1) {
      myTitle = "New Income Sub Head";
    }
    myTitle = "New Expense Sub Head";
    return Get.defaultDialog(
      title: myTitle,
      textConfirm: "Add",
      confirmTextColor: whiteE5,
      buttonColor: accountHead.typeId == 1 ? buttonGreen : buttonRed,
      textCancel: "Cancel",
      cancelTextColor: buttonRed,
      content: Column(children: [accountHeadName()]),
      onConfirm: () async {
        await addSubNewHead(accountHead.id!);
      },
    );
  }

  Widget accountHeadName() {
    return TextFormField(
      controller: _accountHeadController.accountSubHeadCtl,
      decoration: InputDecoration(hintText: "Sub Head Title"),
    );
  }

  Future<void> addSubNewHead(int headId) async {
    if (_accountHeadController.accountSubHeadCtl.text.isEmpty) {
      Get.snackbar("Error", "Head can't be empty");
    } else {
      try {
        await MoneyLogsDatabase.instance.addSubAccountHead(
          AccountSubHead(
            title: _accountHeadController.accountSubHeadCtl.text,
            accountHeadId: headId,
            createdBy: 1,
            createAt: DateFormat('dd-MM-yyyy').format(DateTime.now()),
          ),
        );
        Get.back();
      } catch (e) {
        log(e.toString());
        Get.back();
        Get.snackbar(
          "Error",
          "Sub Head name must be unique",
          colorText: buttonRed,
        );
      } finally {
        _accountHeadController.accountSubHeadCtl.text = "";
        _accountHeadController.update();
      }
    }
  }

  Widget noDataSection() {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [Text("No Sub Head Data Found")],
      ),
    );
  }

  Widget errorSection() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Text('Error occurred', style: const TextStyle(fontSize: 18)),
      ),
    );
  }
}
