import 'package:get/get.dart';

import '../controllers/account_head_controller.dart';

class AccountHeadBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AccountHeadController>(
      () => AccountHeadController(),
    );
  }
}
