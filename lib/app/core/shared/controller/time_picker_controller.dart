import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TimeController extends GetxController {
  Rx<TimeOfDay> selectedTime = TimeOfDay.now().obs;

  chooseTime() async {
    TimeOfDay? newTime = await showTimePicker(
      context: Get.context!,
      initialTime: selectedTime.value,
    );
    if (newTime != null && newTime != selectedTime.value) {
      selectedTime.value = newTime;
    }
  }
}
