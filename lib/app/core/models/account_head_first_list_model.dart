import 'package:get/get.dart';

class AccountHeadFirstList {
  String? name;
  RxBool add;
  int type;

  AccountHeadFirstList({this.name, required this.add, required this.type});
}
