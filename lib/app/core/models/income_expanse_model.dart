import 'income_expense_fields.dart';

class IncomeExpanse {
  int? id;
  late String amount;
  late String details;
  String? transactionDate;
  String? transactionTime;
  String? subHeadId;
  late int createdBy;
  late String createdAt;

  IncomeExpanse({
    this.id,
    required this.amount,
    required this.details,
    this.transactionDate,
    this.transactionTime,
    this.subHeadId,
    required this.createdBy,
    required this.createdAt,
  });

  IncomeExpanse.fromJson(Map<String, dynamic> json) {
    id = json[IncomeExpenseFields.id];
    amount = json[IncomeExpenseFields.amount];
    details = json[IncomeExpenseFields.details];
    transactionDate = json[IncomeExpenseFields.transactionDate];
    transactionTime = json[IncomeExpenseFields.transactionTime];
    subHeadId = json[IncomeExpenseFields.subHeadId];
    createdBy = json[IncomeExpenseFields.createdBy];
    createdAt = json[IncomeExpenseFields.createdAt];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data[IncomeExpenseFields.id] = id;
    data[IncomeExpenseFields.amount] = amount;
    data[IncomeExpenseFields.details] = details;
    data[IncomeExpenseFields.transactionDate] = transactionDate;
    data[IncomeExpenseFields.transactionTime] = transactionTime;
    data[IncomeExpenseFields.subHeadId] = subHeadId;
    data[IncomeExpenseFields.createdBy] = createdBy;
    data[IncomeExpenseFields.createdAt] = createdAt;
    return data;
  }
}
