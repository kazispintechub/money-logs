import 'package:money_logs/app/core/models/type_fields.dart';

class TypeModel {
  int? id;
  String? name;
  String? createdBy;
  String? createdAt;

  TypeModel({this.id, this.name, this.createdBy, this.createdAt});

  TypeModel.fromJson(Map<String, dynamic> json) {
    id = json[TypeFields.id];
    name = json[TypeFields.name];
    createdBy = json[TypeFields.createdBy];
    createdAt = json[TypeFields.createdAt];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    // data['id'] = id;
    data[TypeFields.name] = name;
    data[TypeFields.createdBy] = createdBy;
    data[TypeFields.createdAt] = createdAt;
    return data;
  }
}
