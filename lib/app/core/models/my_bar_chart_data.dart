class MyBarChartData {
  final int id;
  final String headName;
  final double y;
  final int type;

  MyBarChartData({
    required this.id,
    required this.headName,
    required this.y,
    required this.type,
  });
}
