import 'dart:developer';

class ExchangeRate {
  String? result;
  String? termsOfUse;
  String? baseCode;
  ConversionRates? conversionRates;

  ExchangeRate({
    this.result,
    this.termsOfUse,
    this.baseCode,
    this.conversionRates,
  });

  ExchangeRate.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    termsOfUse = json['terms_of_use'];

    baseCode = json['base_code'];
    conversionRates =
        json['rates'] != null ? ConversionRates?.fromJson(json['rates']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['result'] = result;
    data['terms_of_use'] = termsOfUse;
    data['base_code'] = baseCode;
    if (conversionRates != null) {
      log(conversionRates.toString());
      data['rates'] = conversionRates?.toJson();
    }
    return data;
  }
}

class ConversionRates {
  int? bDT;
  double? aED;
  double? aUD;
  double? cAD;
  double? cNY;
  double? eUR;
  double? gBP;
  double? iNR;
  double? jPY;
  double? lKR;
  double? nZD;
  double? oMR;
  double? pKR;
  double? sAR;
  double? uSD;
  double? yER;

  ConversionRates({
    this.bDT,
    this.aED,
    this.aUD,
    this.cAD,
    this.cNY,
    this.eUR,
    this.gBP,
    this.iNR,
    this.jPY,
    this.lKR,
    this.nZD,
    this.oMR,
    this.pKR,
    this.sAR,
    this.uSD,
    this.yER,
  });

  ConversionRates.fromJson(Map<String, dynamic> json) {
    bDT = json['BDT'];
    aED = json['AED'];
    aED = json['AUD'];
    aED = json['CAD'];
    aED = json['CNY'];
    aED = json['EUR'];
    aED = json['GBP'];
    aED = json['INR'];
    aED = json['JPY'];
    aED = json['LKR'];
    aED = json['NZD'];
    aED = json['OMR'];
    aED = json['PKR'];
    aED = json['SAR'];
    aED = json['USD'];
    aED = json['YER'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['BDT'] = bDT;
    data['AED'] = aED;
    data['AUD'] = aED;
    data['CAD'] = cAD;
    data['CNY'] = cNY;
    data['EUR'] = eUR;
    data['GBP'] = gBP;
    data['INR'] = iNR;
    data['JPY'] = jPY;
    data['LKR'] = lKR;
    data['NZD'] = nZD;
    data['OMR'] = oMR;
    data['PKR'] = pKR;
    data['SAR'] = sAR;
    data['USD'] = uSD;
    data['USD'] = yER;
    return data;
  }
}
